<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! post_type_exists('hioice_our_team') ) :

	function hioice_create_our_team_post_type() {

		register_post_type( 'hioice_our_team',
			array(
				'labels' => array(
					'name' => __( 'Our Team', HIOICE_OT_CPT_DOMAIN ),
					'singular_name' => __( 'Team Member', HIOICE_OT_CPT_DOMAIN ),
					'add_new' => __( 'Add New', HIOICE_OT_CPT_DOMAIN ),
					'add_new_item' => __( 'Add New Team Member', HIOICE_OT_CPT_DOMAIN ),
					'edit' => __( 'Edit', HIOICE_OT_CPT_DOMAIN ),
					'edit_item' => __( 'Edit Team Member', HIOICE_OT_CPT_DOMAIN ),
					'new_item' => __( 'New Team Member', HIOICE_OT_CPT_DOMAIN ),
					'view' => __( 'View', HIOICE_OT_CPT_DOMAIN ),
					'view_item' => __( 'View Team Member', HIOICE_OT_CPT_DOMAIN ),
					'search_items' => __( 'Search Our Team', HIOICE_OT_CPT_DOMAIN ),
					'not_found' => __( 'No Team Members found', HIOICE_OT_CPT_DOMAIN ),
					'not_found_in_trash' => __( 'No Team Member found in Trash', HIOICE_OT_CPT_DOMAIN ),
					'parent' => __( 'Parent Team Member', HIOICE_OT_CPT_DOMAIN ),
				),
				'public' => false,
				'show_ui' => true,
				'menu_position' => 20,
				'supports' => array(
					'title', 'editor', 'thumbnail'
				),
				'taxonomies' => array( '' ),
				'menu_icon' => 'dashicons-nametag',
				'has_archive' => false,
				'publicly_queryable' => false,
				'exclude_from_search' => true,
			)
		);

	}
	add_action( 'init', 'hioice_create_our_team_post_type' );

endif;
