<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function hioice_our_team_shortcode( $atts, $content=null ) {

	$defaults = array( 'ids' => '-1' );

	$output = '';
	
	extract( shortcode_atts($defaults, $atts) );

	$query_params = array(
		'post_type' => 'hioice_our_team',
		'post_status' => array ('publish'),
 		'ignore_sticky_posts' => 1,
		'orderby' => 'ID',
		'order' => 'ASC',
		'suppress_filters' => true,
	);

	$member_ids = explode( ',', $ids );

	if ( count($member_ids) > 0 && $member_ids[0] > 0 ) {
		$query_params['post__in'] = $member_ids;
	}

	$team_query = new WP_Query( $query_params );

	if ( $team_query->have_posts() ) {

		$count = $team_query->post_count;

		$team_id = "our-team-" . substr(md5(microtime()),0,40);

		ob_start();
?>
<div class="hioice-our-team-container">
	<div id="<?php echo $team_id ?>" class="team-members" data-members="<? echo $count; ?>">
<?php
		while ( $team_query->have_posts() ) {

			$team_query->the_post();

			$team_member = array(
				'name' => trim( get_the_title( get_the_ID() ) ),
				'headshot' => get_the_post_thumbnail( get_the_ID() ),
				'position' => trim( get_field('hioice_team_member_position') ),
				'description' => get_the_content( get_the_ID() )
				);

?>
	<div class="team-member">
		<div class="team-member-headshot"><?php echo $team_member['headshot']; ?></div>
		<div class="team-member-name"><?php echo esc_html($team_member['name']); ?></div>
		<div class="team-member-position"><?php echo esc_html($team_member['position']); ?></div>
	</div>
<?php
		}

		wp_reset_postdata();
?>
	</div>
</div>
<?php
		$output = ob_get_contents();
		ob_end_clean();
	}

	$inline_js = "Macy.init({ container: '" . $team_id . "' });";

	// Enqueue the masonry library script
	wp_enqueue_scripts('hioice-macy');
	// Add inline JavaScript
	wp_add_inline_script('hioice-macy', $inline_js, 'after');

	return $output;
}


/*
 * Function for registering the [hioice-our-team] shortcode.
 */
function hioice_our_team_register_shortcodes() {
	add_shortcode( 'hioice-our-team', 'hioice_our_team_shortcode' );
}
/* Register shortcodes in 'init'. */
add_action( 'init', 'hioice_our_team_register_shortcodes' );
