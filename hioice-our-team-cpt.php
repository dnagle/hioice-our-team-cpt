<?php
/**
 * Plugin Name: HIOICE Our Team Custom Post Type
 * Description: Adds a Custom Post Type for the 'Maxmedia 2017' theme, used to show team members. 
 * Author: Dan Nagle
 * Text Domain: hioice-our-team
 * Domain Path: /languages/
 * Version: 1.0.1
 * License: GPLv2
 * Bitbucket Plugin URI: https://bitbucket.org/dnagle/hioice-our-team-cpt
 *
 * @package WordPress
 * @author Dan Nagle
 */

if ( !defined( 'HIOICE_OT_CPT_VERSION' ) ) {
	define( 'HIOICE_OT_CPT_VERSION', '1.0.1' ); // Plugin version
}
if ( !defined( 'HIOICE_OT_CPT_DIR' ) ) {
	define( 'HIOICE_OT_CPT_DIR', dirname( __FILE__ ) ); // Plugin directory
}
if ( !defined( 'HIOICE_OT_CPT_URL' ) ) {
	define( 'HIOICE_OT_CPT_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}
if ( !defined( 'HIOICE_OT_CPT_DOMAIN' ) ) {
	define( 'HIOICE_OT_CPT_DOMAIN', 'hioice-our-team' ); // Text Domain
}

/**
 * Load Text Domain
 * This gets the plugin ready for translation
 * 
 * @package Our Team Custom Post Type
 * @since 1.0.0
 */
function hioice_ot_cpt_load_textdomain() {
	load_plugin_textdomain( HIOICE_OT_CPT_DOMAIN, false, HIOICE_OT_CPT_DIR . '/languages/' );
}
add_action('plugins_loaded', 'hioice_ot_cpt_load_textdomain');

require_once HIOICE_OT_CPT_DIR . '/includes/hioice-our-team-post-type.php';

require_once HIOICE_OT_CPT_DIR . '/includes/hioice-our-team-shortcodes.php';

/**
 * Register JavaScript dependencies
 * This registers the macy.js library.
 * 
 * @package Our Team Custom Post Type
 * @since 1.0.0
 */
function hioice_ot_cpt_register_scripts() {
	wp_register_script('hioice-macy', plugin_dir_url(__FILE__) . 'js/macy.js', array(), null, true);
}
add_action('wp_head', 'hioice_ot_cpt_register_scripts');

